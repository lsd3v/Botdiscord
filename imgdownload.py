from google_images_download import google_images_download

def download(keywords,limit,output_directory,size):

    response = google_images_download.googleimagesdownload()
    args = {"keywords":keywords,"limit":limit,"print_urls":True,"output_directory":output_directory,"size":size,"image_directory":'./'}
    absolute_image_paths = response.download(args)
    print(absolute_image_paths)
    return absolute_image_paths[keywords]